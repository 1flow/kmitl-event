<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
 * Authentication routing table
 */
Auth::routes();

/*
 * Essential routing table
 */
Route::get('/', 'HomeController@index');
Route::resource('profile', 'ProfileController', ['only' => ['index', 'update']]);

/*
 * EventController routing table
 */
Route::resource('event', 'EventController');
Route::resource('share', 'ShareController', ['only' => ['show']]);
Route::resource('test', 'PhotoController');

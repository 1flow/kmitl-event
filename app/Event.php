<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Event
 * @package App
 */
class Event extends Model
{
    use Notifiable;

    /**
     * Cast key type to string
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'start_date', 'end_date', 'start_time', 'end_time',
        'location', 'latitude', 'longitude', 'img_url', 'cost', 'contracts_id', 'users_id'
    ];


    /**
     * Making a relationship between Event Model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contract(){
    	return $this->hasOne('App\Contract', 'id', 'contracts_id');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories');
    }

}

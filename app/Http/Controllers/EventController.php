<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;

use Auth;
use App\Event;
use App\Contract;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * The attributes that are custom validator message.
     *
     * @var array
     */
    private $validatorMessages = [

    ];

    /**
     * Display a adding list of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all()->sortBy('created_at');
        return view('event.manage', ['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.fill');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request, [
//            'name' => 'required',
//            'description' => 'required',
//            'start_date' => 'required',
//            'end_date' => 'required',
//            'start_time' => 'required',
//            'end_time' => 'required',
//            'location' => 'required',
//            'price' => 'numeric',
//            'phone_number' => 'required|numeric',
//        ], $this->validatorMessages);

        //Contracts ID and Users ID
        $users_id = Auth::user()->id;
        $contract_id = str_random(5);
        Contract::create([
            'id' => $contract_id,
            'name' => $request->agency,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'website' => $request->website
        ]);

        // Add a image to storage
        if ($request->hasFile('image')) {
            $filename = str_random(30). '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/', $filename);
            $path = asset('storage/'.$filename);
        } else {
            $path = null;
        }

        if ($request->cost == null) {
            $cost = 0;
        } else {
            $cost = $request->cost;
        }
        // Add a event record
        $events_id = str_random(60);
        Event::create([
            'id' => $events_id,
            'name' => $request->activity_name,
            'description' => $request->description,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'location' => $request->location,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'cost' => $cost,
            'img_url' => $path,
            'contracts_id' => $contract_id,
            'users_id' => $users_id,
        ]);

        // Add categories to events relation
        foreach($request->categories as $selected) {
            DB::table('events_categories')->insert([
                'events_id' => $events_id,
                'categories_id' => $selected,
            ]);
        }

        return redirect()->route('event.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Event::where('id', $id)->with('contract')->first();
        return view('event.detail', ['detail' => $detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Event::where('id', $id)->with('contract')->first();
        return view('event.edit', ['detail' => $detail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Contracts ID and Users ID
        $contract_id = DB::table('events')->select('contracts_id')->where('id', $id)->first();
        Contract::where('id', $contract_id->contracts_id)->update([
            'name' => $request->agency,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'website' => $request->website
        ]);

        // Add a image to storage
        if ($request->hasFile('image')) {
            $filename = str_random(30). '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/', $filename);
            $path = asset('storage/'.$filename);
        } else {
            $path = null;
        }

        if ($request->cost == null) {
            $cost = 0;
        } else {
            $cost = $request->cost;
        }

        // Add a event record
        Event::where('id', $id)->update([
            'id' => $id,
            'name' => $request->activity_name,
            'description' => $request->description,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'location' => $request->location,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'cost' => $request->cost,
            'img_url' => $path,
        ]);

        // Add categories to events relation
        DB::table('events_categories')->where('events_id', $id)->delete();
        foreach($request->categories as $selected) {
            DB::table('events_categories')->where('events_id', $id)->update([
                'categories_id' => $selected,
            ]);
        }

        return redirect()->route('event.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contract_id = DB::table('events')->select('contracts_id')->where('id', $id)->first();
        DB::table('contracts')->where('id', $contract_id->contracts_id)->delete();
        DB::table('events')->where('id', $id)->delete();
        return redirect()->route('event.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Http\Requests;

class ProfileController extends Controller
{
    /**
     * The attributes that are custom validator message.
     *
     * @var array
     */
    private $validatorMessages = [
        'password.confirmed' => 'รหัสผ่านไม่ตรงกัน',
        'password.min' => 'รหัสผ่านต้องมีมากกว่า 6 ตัวขึ้นไป'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'user' => Auth::user()
        ];

        return view('profile', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'confirmed|min:6'
        ], $this->validatorMessages);

        User::where('id', $id)->update(['password' => bcrypt($request->password)]);
        Auth::logout();
        return redirect(url('/login'));
    }
}
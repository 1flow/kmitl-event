@extends('layouts.app')
@section('blade_name')
      <h1>ข้อมูลกิจกรรมในฐานข้อมูล
            <small>Event Description</small>
      </h1>
@endsection
@section('content')
      <div class="box box-success">
            <div class="box-header">
                  <h3 class="box-title">รายละเอียดกิจกรรม</h3>
            </div>
            <div class="box-body">
                  <strong><i class="fa fa-book margin-r-5"></i> ชื่อกิจกรรม</strong>
                  <p class="text-muted">
                        {{ $detail->name }}
                  </p>
                  <hr>
                  <strong><i class="fa fa-pencil margin-r-5"></i> รายละเอียดของกิจกรรม</strong>
                  <p class="text-muted">
                        {{ $detail->description }}
                  </p>
                  <hr>
                  <strong><i class="fa fa-clock-o margin-r-5"></i> วันที่จัดกิจกรรม</strong>
                  <p class="text-muted">
                        {{ $detail->start_date }} ถึง {{ $detail->end_date }}
                  </p>
                  <hr>
                  @if ($detail->cost != 0)
                        <strong><i class="fa fa-dollar margin-r-5"></i> ราคาบัตร</strong>
                        <p class="text-muted">
                              {{ $detail->cost }} บาท
                        </p>
                        <hr>
                  @else
                        <strong><i class="fa fa-dollar margin-r-5"></i> ราคาบัตร</strong>
                        <p class="text-muted">
                              ฟรี
                        </p>
                        <hr>
                  @endif
                  <strong><i class="fa fa-map-marker margin-r-5"></i> สถานที่จัดกิจกรรม</strong>
                  <p class="text-muted">
                        {{ $detail->location }}
                  </p>
                  <hr>
                  <strong><i class="fa fa-tags margin-r-5"></i> Tag</strong>
                  <p>
                        <span class="label label-danger">UI Design</span>
                        <span class="label label-success">Coding</span>
                        <span class="label label-info">Javascript</span>
                        <span class="label label-warning">PHP</span>
                        <span class="label label-primary">Node.js</span>
                  </p>
                  <hr>
            </div>
      </div>

      <div class="box box-success">
            <div class="box-header">
                  <h3 class="box-title">รายละเอียดผู้จัดกิจกรรม</h3>
            </div>
            <div class="box-body">
                  <strong><i class="fa fa-users margin-r-5"></i>กลุ่มผู้จัดกิจกรรม</strong>
                  <p class="text-muted">
                        {{ $detail->contract->name }}
                  </p>
                  <hr>
                  <strong><i class="fa fa-phone margin-r-5"></i>เบอร์โทรศัพท์</strong>
                  <p class="text-muted">
                        {{ $detail->contract->phone_number }}
                  </p>
                  <hr>
                  <strong><i class="fa fa-envelope margin-r-5"></i>E-mail address</strong>
                  <p class="text-muted">
                        {{ $detail->contract->email }}
                  </p>
                  <hr>
                  @if ($detail->facebook != null)
                        <strong><i class="fa fa-facebook-official margin-r-5"></i>Facebook</strong>
                        <a class="text-muted" href="{{ $detail->facebook_url }}">
                              {{ $detail->contract->facebook_url }}
                        </a>
                        <hr>
                  @endif
                  @if ($detail->line != null)
                        <strong><i class="fa fa-external-link margin-r-5"></i>Line ID</strong>
                        <p class="text-muted">
                              {{ $detail->contract->line_id }}
                        </p>
                        <hr>
                  @endif
                  @if ($detail->twitter != null)
                        <strong><i class="fa fa-twitter margin-r-5"></i>Twitter</strong>
                        <a class="text-muted" href="{{ $detail->twitter_url }}">
                              {{ $detail->contract->twitter_url }}
                        </a>
                        <hr>
                  @endif
            </div>
      </div>
      <a role="button" class="btn btn-success btn-block" href="{{ url('/event.manage') }}">กลับสู่หน้าจัดการกิจกรรม</a>
@endsection
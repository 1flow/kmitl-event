@extends('layouts.app')
@section('css')
    <!-- Bootstrap Datepicker style -->
    <link rel="stylesheet" href="{{ asset('css/datepicker3.css') }}">
    <!-- Bootstrap Timepicker style -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-timepicker.min.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/css/bootstrap-responsive.css') }}" rel="stylesheet">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyB_vaUc8yJZXXtsg1S0hcWkh4Rl7obcLzE"></script>
    <script type="text/javascript">
        var map;
        var marker;
        var mygeocode = new google.maps.LatLng(13.730190, 100.779191);
        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        function initialize() {
            var mapOptions = {
                zoom: 17,
                center: mygeocode,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            marker = new google.maps.Marker({
                map: map,
                position: mygeocode,
                draggable: true
            });
            geocoder.geocode({'latLng': mygeocode}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection
@section('blade_name')
    <h1>จัดการกิจกรรม
        <small>Event Management</small>
    </h1>
@endsection
@section('content')
    <form class="form-horizontal" role="form" method="POST" action="{{ route('event.update', $detail->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}

        <div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-header col-md-10">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <h4 class="box-title">ข้อมูลกิจกรรม</h4>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-12"><br>
                                <label style="color: #122b40">ชื่อกิจกรรม</label>
                                <input type="text" class="form-control" id="name_activity" name="activity_name" value="{{ $detail->name }}"
                                       placeholder="IT CAMP 13">
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                            <div class="col-md-12">
                                <label style="color: #122b40">รายละเอียดกิจกรรม</label>
                                <textarea type="text" class="form-control" id="description" name="description" rows="6"
                                          placeholder="เป็นกิจกรรมที่ให้น้องๆ ม.ปลายได้มาเข้าค่าย โดยมี 5 ค่าย....">{{ $detail->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                            <div class="col-md-6">
                                <label style="color: #122b40">วันเริ่มกิจกรรม</label>
                                <input type="date" class="datepicker form-control" id="start_date" name="start_date" value="{{ $detail->start_date }}"
                                       placeholder="2016-12-10"/>
                            </div>
                            <div class="col-xs-6">
                                <label style="color: #122b40">วันสิ้นสุดกิจกรรม</label>
                                <input type="date" class="datepicker form-control" id="end_date" name="end_date" value="{{ $detail->end_date }}"
                                       placeholder="2016-12-14"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                            <div class="col-md-6 bootstrap-timepicker">
                                <label style="color: #122b40">เวลาเริ่มกิจกรรม</label>
                                <input type="text" class="form-control timepicker" id="start_time" name="start_time" value="{{ $detail->start_time }}"
                                       placeholder="15.00">
                            </div>
                            <div class="col-md-6 bootstrap-timepicker">
                                <label style="color: #122b40">เวลาสิ้นสุดกิจกรรม</label>
                                <input type="text" class="form-control timepicker" id="end_time" name="end_time" value="{{ $detail->end_time }}"
                                       placeholder="22.00">
                            </div>
                        </div>
                        {{--Map--}}
                        <div class="form-group">
                            <hr>
                            <div class="col-md-12">
                                <label style="color: #122b40">ชื่อสถานที่</label>
                                <input type="text" class="form-control" id="place_name" name="location" value="{{ $detail->location }}"
                                       placeholder="คณะเทคโนโลยีสารสนเทศ">
                            </div>
                            <div class="col-md-12"><br>
                                <label style="color: #122b40">แผนที่</label>
                                <p class="help-block">(ลากหมุดไปยังตำแหน่งที่ต้องการ)</p>
                                <div id="map_canvas" style="width: 100%; height: 460px;"></div>
                                <input type="hidden" id="latitude" name="latitude"/>
                                <input type="hidden" id="longitude" name="longitude"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-success">
                    <!-- Multiple Radios (inline) -->
                    <div class="box-header">
                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                        <h4 class="box-title">ส่วนเข้าร่วม และรูปภาพ</h4>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label style="color: #122b40">ราคาเข้าร่วม</label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="radio-inline">
                                            <input type="radio" name="price" id="price" value="free" <?php if ($detail->cost == 0) echo "checked";?>>ฟรี</label></div>
                                    <div class="col-md-9">
                                        <label class="radio-inline">
                                            <input type="radio" name="price" id="price" value="cost" <?php if ($detail->cost != 0) echo "checked";?>>ราคา
                                        </label>
                                        <input class="col-md-offset-1" id="price" name="price" placeholder="800" value="{{ $detail->cost }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <hr>
                                <label style="color: #122b40">อยู่ในหมวดหมู่ (สามารถเลือกได้มากกว่า 1)</label>
                                <div class="row checkbox">
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="5UOlxqvJv8tqVTbvSx115Yfs4OGVxrhZZJ2gs9dIkH2bPAb1gh5D0CN82tkm">วิชาการ</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="dLwyOdU9vIofWLezUR72rovcQnM7WnIHGePI7xsgx7sMFK6qEpcZ6aJABhpj">ดนตรี</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="gRX4DFtYoKbLJmen5F9Mh5iDlgBoM3L0ShMZILsc0xGQKldHZ1yc1VsVnlx8">กีฬา</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="rv9wr3pkDehhHXRKApizu6JQzi7tOrzSRzXap7lUVgrrpm6JLXtBoLl4lNlQ">นิทรรศการ</label>
                                    </div>
                                </div>
                                <div class="row checkbox">
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="sXCE3nrHODyhSVGgZhMYw55xRRotZYQczsm609RTGjPhYbGnlL6A0EUSmUuy">พิธีการ</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="tTG31VaR9ZzMQgjzy9SD94AOoFFsiOtN2Zod8xf68rpIx4cr67481FG4ZDvT">ค่าย</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="Ob8hKwjyJSWp2CILDnStfYJ64JVeaPuqgmN8vd0omFl2asqvYSMYdkpF5egn">ค้าขาย</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="CoF73uS8ItYn2HpDSoLbrbFtK7ruvVCx7KpR1VC6YaLZFaDnvuX2i8OfCukp">ชมรม</label>
                                    </div>
                                </div>
                                <div class="row checkbox">
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="eqNIggarpAOhlUkIPqjMNeN8CoEJ2ONi3zFqyrqmyyxepHkOv3heubvJ8Eb2">ศาสนา</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="vRifEVibyI4Zx67aCDZPnoPbTtanu0jhigtu1z7PdEOWWr8guihqTKfic8ov">ประกวดการแข่งขัน</label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline"><input type="checkbox" name="categories[]" value="Tv1HwLD0q53yoOz3X3eux8l6s0pSeS4SZA8RmLIasBslZGbB9hRONYThGTRP">บำเพ็ญประโยชน์</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9">
                                <hr>
                                <label style="color: #122b40">เพิ่มรูปกิจกรรม</label>
                                <input type='file' name="image" onchange="readURL(this);"/>
                                <p class="help-block">ไฟล์ขนาดไม่เกิน 2 เมกะไบต์</p>
                                <img class="img-responsive" id="img" src="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <h4 class="box-title">ติดต่อสอบถาม หรือติดตามกิจกรรม</h4>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-7">
                                <label style="color: #122b40">ชื่อหน่วยงาน หรือผู้ติดต่อ</label>
                                <input type="text" class="form-control" id="agency" name="agency" value="{{ $detail->contract->name }}"
                                       placeholder="พี่ซัน">
                            </div>
                            <div class="col-md-5">
                                <label style="color: #122b40">โทรศัพท์</label>
                                <input type="tel" class="form-control " id="phone_number" name="phone_number" value="{{ $detail->contract->phone_number }}"
                                       placeholder="090-986-4114">
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                            <div class="col-md-6">
                                <label style="color: #122b40">Facebook</label>
                                <input type="text" class="form-control" id="facebook_url" name="facebook_url" value="{{ $detail->contract->facebook_url }}"
                                       placeholder="fb.com/wjsunshine">
                            </div>
                            <div class="col-md-6">
                                <label style="color: #122b40">Twitter</label>
                                <input type="text" class="form-control " id="twitter_url" name="twitter_url" value="{{ $detail->contract->twitter_url }}"
                                       placeholder="@wjsunshine">
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                            <div class="col-md-12">
                                <label style="color: #122b40">E-mail</label>
                                <input type="email" class="form-control " id="email" name="email" value="{{ $detail->contract->email }}"
                                       placeholder="Love_sun.ny@hotmail.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                            <div class="col-md-12">
                                <label style="color: #122b40">Website</label>
                                <input type="text" class="form-control " id="website" name="website" value="{{ $detail->contract->website }}"
                                       placeholder="itcamp13.it.kmitl.ac.th">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-success center-block">บันทึก
                <i class="fa fa-arrow-circle-right"></i></button>
        </div>
    </form>
@endsection
@section('js')
    <!-- Bootstrap Datepicker -->
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.th.js') }}"></script>
    <script type="text/javascript">
        $('input.datepicker').datepicker({
            autoclose: true,
            language: 'th',
            format: 'yyyy-mm-dd',
        });
    </script>
    <!-- Boostrap Timepicker -->
    <script src="{{ asset('js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript">
        $("input.timepicker").timepicker({
            showInputs: false,
            showMeridian: false
        });
    </script>
    {{--Pic--}}
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img')
                        .attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
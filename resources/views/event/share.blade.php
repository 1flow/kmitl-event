<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KMITL Event</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/share-style.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyB_vaUc8yJZXXtsg1S0hcWkh4Rl7obcLzE"></script>

    <link rel="stylesheet" href="{{ asset('css/material.grey-orange.min.css') }}">
    <style type="text/css">
        .demo-blog .on-the-road-again .mdl-card__media {
            background-image: url('{{ $detail->img_url }}');
        }

    </style>

    <script type="text/javascript">
        var map;
        var marker;
        var mygeocode = new google.maps.LatLng(13.730190, 100.779191);
        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        function initialize() {
            var mapOptions = {
                zoom: 17,
                center: mygeocode,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            marker = new google.maps.Marker({
                map: map,
                position: mygeocode,
                draggable: true
            });
            geocoder.geocode({'latLng': mygeocode}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <style>
        #map {
            width: 100%;
            height: 400px;
        }
    </style>

</head>
<body>

<div class="demo-blog mdl-layout mdl-js-layout has-drawer is-upgraded">
    <main class="mdl-layout__content">
        <div class="demo-blog__posts mdl-grid">
            <img class="logo-kmitl" src="{{ asset('img/logo-web.png') }}">

            <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
                <div class="mdl-card__media mdl-color-text--grey-50 min-size">
                    <h3>{{ $detail->name }}</h3>
                </div>
                <div class="mdl-color-text--grey-600 mdl-card__supporting-text">
                    {{ $detail->description }}
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-calendar fa-3x"></i>
                            <div>
                                <span>{{ $detail->start_date }} ถึง {{ $detail->end_date }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-clock-o fa-3x"></i>
                            <div>
                                <span>{{ $detail->start_time }} ถึง {{ $detail->end_time }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-money fa-3x"></i>
                            <div>
                                @if ($detail->cost != 0)
                                        {{ $detail->cost }} บาท
                                @else
                                        ฟรี!!!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div id="map_canvas" style="width: 100%; height: 460px;"></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-user fa-3x"></i>
                            <div>
                                <span>{{ $detail->contract->name }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-envelope fa-3x"></i>
                            <div>
                                <span><a href="mailto:{{ $detail->contract->email }}">{{ $detail->contract->email }}</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-phone-square fa-3x"></i>
                            <span>{{ $detail->contract->phone_number }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-external-link fa-3x"></i>
                            <div>
                                <span>{{ $detail->contract->website }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-facebook fa-3x"></i>
                            <div>
                                <span><a href="{{ $detail->contract->facebook }}">{{ $detail->contract->facebook }}</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
                            <i class="fa fa-twitter fa-3x"></i>
                            <span><a href="{{ $detail->contract->twitter }}">{{ $detail->contract->twitter }}</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="mdl-layout__obfuscator"></div>
</div>
<!-- jQuery 2.2.3 -->
<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Material Design -->
<script src="{{ asset('js/material.min.js') }}"></script>
<script type="text/javascript">
    var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(54.00, -3.00),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var geocoder = new google.maps.Geocoder();
    var address = '3410 Taft Blvd  Wichita Falls, TX 76308';
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
</script>
Raw

</body>
</html>
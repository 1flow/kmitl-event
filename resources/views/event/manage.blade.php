@extends('layouts.app')
@section('blade_name')
    <h1>กิจกรรมที่ดูแลทั้งหมด
        <small>Event Management</small>
    </h1>
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-body">
            @if($events->isEmpty())
                    <h3 class="text-center">ขออภัย ไม่มีกิจกรรมในระบบ</h3>
                    <br>
                    <a role="button" class="btn btn-success btn-block" href="{{ route('event.create') }}">เพิ่มกิจกรรม</a>
            @else
                <div class="callout callout-danger">
                    <h4>คำเตือน!</h4>
                    <ul>
                        <li>ระบบจะแสดงเฉพาะกิจกรรมที่คุณกำกับดูแลอยู่เท่านั้น</li>
                        <li>เมื่อทำการแก้ไขหรือลบกิจกรรมแล้ว จะไม่สามารถย้อนกลับได้</li>
                    </ul>
                </div>
                <hr>
                @foreach($events as $event)
                    <div class="row">
                        <div class="col-md-9 col-xs-12 lead">
                            {{ $event->name }}
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="btn-group btn-group-justified">
                                <a role="button" class="btn btn-block btn-success" href="{{ route('event.show', $event->id) }}">ดูข้อมูล</a>
                                <a role="button" class="btn btn-info"
                                   href="{{ route('event.edit', $event->id) }}">แก้ไข</a>
                                <a role="button" class="btn btn-danger" data-toggle="modal" data-target="#{{ $event->id }}">ลบ</a>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade alert-modal" id="{{ $event->id }}" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">ต้องการลบกิจกรรม "{{ $event->name }}"</h4>
                                </div>
                                <div class="modal-body">
                                    หากลบแล้วจะไม่สามารถกู้คืนข้อมูลได้อีก ยืนยัน?
                                </div>
                                <div class="modal-footer">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('event.destroy', $event->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                                        <button type="submit" class="btn btn-primary">ยืนยัน</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('.alert_modal').modal({
            keyboard: false
        })
    </script>
@endsection
@extends('layouts.app')

<!-- Main Content -->
@section('content')
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">กู้คืนรหัสผ่าน</h3>
        </div>
        <div class="box-body">
            @if (session('status'))
                {{ session('status') }}
                <div class="callout callout-info">
                    <h4>ผลลัพธ์!</h4>
                    <ul>
                        <li>{{ session('status') }}</li>
                    </ul>
                </div>
            @else
                <div class="callout callout-info">
                    <h4>ข้อมูลควรทราบ!</h4>
                    <ul>
                        <li>เมื่อระบบพบว่า E-mail address ตรงกับฐานข้อมูล
                            ระบบจะทำการส่งอีเมล์เพื่อทำการกู้รหัสผ่านใหม่
                        </li>
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-default" id="sendEmail">ยืนยัน
                        <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection

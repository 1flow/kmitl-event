@extends('layouts.app')
@section('blade_name')
    <h1>ข้อมูลบัญชี
        <small>Profile</small>
    </h1>
@endsection
@section('content')
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">แก้ไขรหัสผ่าน</h3>
        </div>
        <div class="box-body">
            <div class="callout callout-danger">
                <h4>คำเตือน!</h4>
                <ul>
                    <li>เพื่อความปลอดภัยของบัญชีผู้ใช้ หากทำการเปลี่ยนรหัสผ่านสำเร็จแล้ว ระบบจะแจ้งข้อความให้ทราบ แล้วจะทำการออกจากระบบทันที</li>
                </ul>
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ route('profile.update', $user->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">รหัสผ่านใหม่</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">ยืนยันรหัสผ่าน</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               required>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <button type="submit" class="pull-right btn btn-default">ยืนยัน
                        <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection
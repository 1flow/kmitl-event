@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
@endsection
@section('blade_name')
<h1>ภาพรวมระบบ
	<small>Dashboard</small>
</h1>
@endsection
@section('content')
 <!-- ROW1 -->
<div class="row">
 <!-- PIC 1 -->
	<div class="col-xs-12 col-md-6 col-lg-4" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
		<div class="box box-solid">
		<div class="material"> 
	<img class="img-responsive" src="{{ asset('img/parinya.jpg')}}">
	        
	<div class="material-footer">
		        <h2 class="info-box-text">IT CAMP12</h2>

		        <p class="info-box-text">ไอทีแคมป์ครั้งที่ 12. ยินดีต้อนรับน้องๆ เหล่านักผจญภัยทั้งหลาย พร้อมที่จะไปผจญภัยกันหรือยัง? ที่นี่คือที่สำหรับรับสมัครนักผจญภัยที่จะมาร่วมเดินทางไปกับพวกพี่ในค่าย IT CAMP</p>
		        </div>
	        </div>
		</div>
    </div>
 <!-- PIC 2 -->
    <div class="col-xs-12 col-md-6 col-lg-4" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal2">
		<div class="box box-solid">
		<div class="material"> 
	<img class="img-responsive" src="{{ asset('img/train.jpg')}}">
		        <div class="material-footer">
		        <h2 class="info-box-text">มหกรรมลดทะลุพิกัด</h2>

		        <p class="info-box-text">มหกรรมลดทะลุพิกัด รวบรวมสินค้าหลากหลายแบรนด์ชั้นนำกว่าล้านรายการ จากห้างสรรพสินค้า และร้านค้าชั้นนำในศูนย์การค้ามาลดราคา อาทิ เครื่องเสียง - เครื่องใช้ไฟฟ้า ของตกแต่งบ้าน เครื่องนอน เครื่องครัว อุปกรณ์เครื่องมือสำหรับช่าง เสื้อผ้าแฟชั่นแบรนด์ชั้นนำ ทั้งผู้ชาย – ผู้หญิง อุปกรณ์กีฬา รองเท้า เสื้อผ้ากีฬา นอกจากนี้ยังมีโซนอาหารแสนอร่อย และอื่นๆ อีกมากมาย ช้อปกันให้เพลินกับโปรโมชั่นร่วมกับบัตรเครดิตชั้นนำ พิเศษสุดๆ กับช่วงเวลาแห่งการช้อปปิ้งสินค้า Super Shock Price ลดต่ำกว่าทุนทุกวัน 3 รอบ/ วัน ถึงก่อนมีสิทธิ์ก่อน กับสินค้าไฮไลท์ราคาพิเศษสุดๆ เมื่อช้อปภายในงานลุ้นทุกวันกับของรางวัลมากมายภายในงาน</p>
		        </div>
	        </div>
		</div>
    </div>
<!-- PIC 3 -->
    <div class="col-xs-12 col-md-6 col-lg-4" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal3">
		<div class="box box-solid">
		<div class="material"> 
	<img class="img-responsive" src="{{ asset('img/parinya2.jpg')}}">
		        <div class="material-footer">
		        <h2 class="info-box-text">ลิปตา ออน ไอซ์</h2>

		        <p class="info-box-text">ลิปตา” ศิลปินคู่แนวป๊อป อาร์แอนด์บี ที่โด่งดังสุดๆ ตั้งแต่เพลงเปิดตัว “ฝืน” และ “กอดตัวเอง” จากผลงานอัลบั้มแรก หลังจากนั้นตลอด 10 ปี ที่ผ่านมา ลิปตามีผลงานมาแล้ว 5 อัลบั้ม มีเพลงที่โด่งดังโดนใจแฟนเพลงมากมาย ไม่ว่าจะเป็น ลมหายใจของเมื่อวาน, ปฎิเสธอย่างไร, อ่อนแอ, ยัง, ลองคุย, แค่รู้ว่ารัก, ไม่ต้องขอโทษ, ใจอยากบอกรัก, ไม่คิด, รวมถึงเพลงประกอบละครดัง อย่าง “ไปอยู่ที่ไหนมา” จากเรื่อง ก๊วนคานทองกับแก๊งพ่อปลาไหล, “ไม่คิด” จากเรื่อง นางสาวทองสร้อย, “เหงา” จากเรื่อง วัยแสบสาแหรกขาด นอกจากทั้ง 2 หนุ่ม จะสร้างสรรค์เพลงโดนๆออกมามากมายแล้ว ความโดดเด่นอีกอย่างหนึ่งของวงลิปตาคือบุคลิกกวนๆ บวกกับมุขฮาๆ โดนใจ ที่สามารถเรียกรอยยิ้มและเสียงหัวเราะจากแฟนเพลงทุกครั้งที่พวกเขาขึ้นเวที</p>
		        </div>
	        </div>
		</div>
    </div>
</div>
 <!-- ROW2 -->
<div class="row">
 <!-- PIC 4 -->
	<div class="col-xs-12 col-md-6 col-lg-4">
		<div class="box box-solid">
		<div class="material"> 
	<img class="img-responsive" src="{{ asset('img/tai.jpg')}}">
		        <div class="material-footer">
		        <h2 class="info-box-text">ประดับไทด์</h2>

		        <p class="info-box-text">เป็นงานวันรับไทด์ของสถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</p>
		        </div>
	        </div>
		</div>
    </div>
 <!-- PIC 5 -->
    <div class="col-xs-12 col-md-6 col-lg-4">
		<div class="box box-solid">
		<div class="material"> 
	<img class="img-responsive" src="{{ asset('img/parinya2.jpg')}}">
		        <div class="material-footer">
		        <h2 class="info-box-text">ประดับไทด์</h2>

		        <p class="info-box-text">เป็นงานวันรับไทด์ของสถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</p>
		        </div>
	        </div>
		</div>
    </div>

 <!-- PIC 6 -->
    <div class="col-xs-12 col-md-6 col-lg-4">
		<div class="box box-solid">
		<div class="material"> 
	<img class="img-responsive" src="{{ asset('img/parinya4.jpg')}}">
		        <div class="material-footer">
		        <h2 class="info-box-text">ประดับไทด์</h2>

		        <p class="info-box-text">เป็นงานวันรับไทด์ของสถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง</p>
		        </div>
	        </div>
		</div>
    </div>
</div>

<!-- Modal 1 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	<img class="img-responsive" src="{{ asset('img/parinya.jpg')}}">
    	<div class="material-information">
    	<h2 class="info-box-text">IT CAMP12</h2>
		        <p>ไอทีแคมป์ครั้งที่ 12. ยินดีต้อนรับน้องๆ เหล่านักผจญภัยทั้งหลาย พร้อมที่จะไปผจญภัยกันหรือยัง? ที่นี่คือที่สำหรับรับสมัครนักผจญภัยที่จะมาร่วมเดินทางไปกับพวกพี่ในค่าย IT CAMP</p>
		        <hr>
		        <a role="button" class="btn btn-block btn-flat" href="http://localhost:8000/event/1">ดูข้อมูลเพิ่มเติม</a>
		       	
		</div>
    </div>
  </div>
</div>
<!-- Modal 2 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	<img class="img-responsive" src="{{ asset('img/train.jpg')}}">
      	<div class="material-information">
      	<h2 class="info-box-text">มหกรรมลดทะลุพิกัด ครั้งที่ 16</h2>

		        <p>มหกรรมลดทะลุพิกัด รวบรวมสินค้าหลากหลายแบรนด์ชั้นนำกว่าล้านรายการ จากห้างสรรพสินค้า และร้านค้าชั้นนำในศูนย์การค้ามาลดราคา อาทิ เครื่องเสียง - เครื่องใช้ไฟฟ้า ของตกแต่งบ้าน เครื่องนอน เครื่องครัว อุปกรณ์เครื่องมือสำหรับช่าง เสื้อผ้าแฟชั่นแบรนด์ชั้นนำ ทั้งผู้ชาย – ผู้หญิง อุปกรณ์กีฬา รองเท้า เสื้อผ้ากีฬา นอกจากนี้ยังมีโซนอาหารแสนอร่อย และอื่นๆ อีกมากมาย ช้อปกันให้เพลินกับโปรโมชั่นร่วมกับบัตรเครดิตชั้นนำ พิเศษสุดๆ กับช่วงเวลาแห่งการช้อปปิ้งสินค้า Super Shock Price ลดต่ำกว่าทุนทุกวัน 3 รอบ/ วัน ถึงก่อนมีสิทธิ์ก่อน กับสินค้าไฮไลท์ราคาพิเศษสุดๆ เมื่อช้อปภายในงานลุ้นทุกวันกับของรางวัลมากมายภายในงาน</p>
		        <hr>
		        <a role="button" class="btn btn-block btn-flat" href="http://localhost:8000/event/1">ดูข้อมูลเพิ่มเติม</a>
		</div>
    </div>
  </div>
</div>

<!-- Modal 3 -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	<img class="img-responsive" src="{{ asset('img/parinya2.jpg')}}">
      	<div class="material-information">
      	<h2 class="info-box-text">ลิปตา ออน ไอซ์</h2>

		        <p>ลิปตา” ศิลปินคู่แนวป๊อป อาร์แอนด์บี ที่โด่งดังสุดๆ ตั้งแต่เพลงเปิดตัว “ฝืน” และ “กอดตัวเอง” จากผลงานอัลบั้มแรก หลังจากนั้นตลอด 10 ปี ที่ผ่านมา ลิปตามีผลงานมาแล้ว 5 อัลบั้ม มีเพลงที่โด่งดังโดนใจแฟนเพลงมากมาย ไม่ว่าจะเป็น ลมหายใจของเมื่อวาน, ปฎิเสธอย่างไร, อ่อนแอ, ยัง, ลองคุย, แค่รู้ว่ารัก, ไม่ต้องขอโทษ, ใจอยากบอกรัก, ไม่คิด, รวมถึงเพลงประกอบละครดัง อย่าง “ไปอยู่ที่ไหนมา” จากเรื่อง ก๊วนคานทองกับแก๊งพ่อปลาไหล, “ไม่คิด” จากเรื่อง นางสาวทองสร้อย, “เหงา” จากเรื่อง วัยแสบสาแหรกขาด นอกจากทั้ง 2 หนุ่ม จะสร้างสรรค์เพลงโดนๆออกมามากมายแล้ว ความโดดเด่นอีกอย่างหนึ่งของวงลิปตาคือบุคลิกกวนๆ บวกกับมุขฮาๆ โดนใจ ที่สามารถเรียกรอยยิ้มและเสียงหัวเราะจากแฟนเพลงทุกครั้งที่พวกเขาขึ้นเวที</p>
		        <hr>
		        <a role="button" class="btn btn-block btn-flat" href="http://localhost:8000/event/1">ดูข้อมูลเพิ่มเติม</a>
		</div>
    </div>
  </div>
</div>

@endsection
@section('js')

@endsection
